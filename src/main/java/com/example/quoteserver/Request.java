package com.example.quoteserver;

import java.util.Objects;

public class Request {
    private static int requestsNum = 0;
    private final int id;
    private final int securityId;
    private final int quantity;
    private final boolean buy;

    public Request(final int securityId, final int quantity, final boolean buy) {
        this.securityId = securityId;
        this.quantity = quantity;
        this.buy = buy;
        id = incrementRequestsCount();
    }

    private static synchronized int incrementRequestsCount() {
        return requestsNum++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return id == request.id &&
                securityId == request.securityId &&
                quantity == request.quantity &&
                buy == request.buy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, securityId, quantity, buy);
    }
}
