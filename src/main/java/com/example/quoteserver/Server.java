package com.example.quoteserver;

import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePrice;
import com.example.marketmaker.ReferencePriceListener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Server {
    public static final int PORT = 22000;

    private final QuoteCalculationEngine qEngine;
    private final ReferencePriceListener referencePriceListener;
    private final ReferencePrice referencePrice;
    private Map<Request, Double> quotes;

    public Server(QuoteCalculationEngine qEngine) {
        if (qEngine == null) {
            System.out.println("QuoteCalculationEngine is null!");
        }
        this.qEngine = qEngine;
        referencePriceListener = new ReferencePriceListener();
        referencePrice = new ReferencePrice();
        referencePrice.subscribe(referencePriceListener);
        quotes = new ConcurrentHashMap<Request, Double>();
        new Thread(new Runnable() {
            public void run() {
                referencePrice.startSource();
            }
        }).start();
    }

    public void startServer() throws Exception {
        String clientRequest;
        ServerSocket welcomeSocket = new ServerSocket(PORT);
        System.out.println("Server started");
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            BufferedReader inFromClient =
                    new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            clientRequest = inFromClient.readLine();
            if (clientRequest == null || clientRequest.isEmpty()) {
                continue;
            }
            retrieveQuote(connectionSocket, clientRequest);
        }
    }

    private double retrieveRefPrice(final int securityId) {
        double refPx = -1;
        while (refPx == -1) {
            refPx = referencePriceListener.getRefPrice(securityId);
        }
        return refPx;
    }

    private void retrieveQuote(final Socket connectionSocket, final String clientRequest/*, final int securityId, final double refPx, final boolean buy, final int quantity*/) {
        if (quotes == null) {
            System.out.println("Quotes store is null!");
        }
        new Thread(new Runnable() {
            public void run() {
                System.out.println("FROM CLIENT: " + clientRequest);
                DataOutputStream outToClient = null;
                try {
                    outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                RequestMessage requestMessage = new RequestMessage(clientRequest);
                RequestMessage.TradeInstruction instruction = requestMessage.getTradeInstruction();
                boolean buy = tradeInstructionToBool(instruction);
                int securityId = requestMessage.getSecurrityId();
                int quantity = requestMessage.getQuantity();
                Request request = new Request(securityId, quantity, buy);
                referencePriceListener.addSecurityId(securityId);
                double refPx = retrieveRefPrice(securityId);
                quotes.put(request, qEngine.calculateQuotePrice(securityId, refPx, buy, quantity));
                try {
                    assert outToClient != null;
                    outToClient.writeBytes(Double.toString(quotes.get(request)) + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private boolean tradeInstructionToBool(final RequestMessage.TradeInstruction side) {
        return side.equals(RequestMessage.TradeInstruction.BUY);
    }
}