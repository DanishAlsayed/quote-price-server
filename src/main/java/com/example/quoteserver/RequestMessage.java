package com.example.quoteserver;

public class RequestMessage {
    private static final int MIN_QUANTITY = 1;
    private static final int MIN_SECURITY_ID = 0; //Assuming security ids are only positive
    private static final String MSG_DELIMITER = " ";
    private static final int NUM_PARAMETERS = 3;

    public int getSecurrityId() {
        return securrityId;
    }

    public int getQuantity() {
        return quantity;
    }

    public TradeInstruction getTradeInstruction() {
        return tradeInstruction;
    }

    public enum TradeInstruction {
        BUY, SELL;
    }

    private final int SECURITYID_INDEX = 0;
    private final int T_INSTRUCTION_INDEX = 1;
    private final int QUANTITY_INDEX = 2;

    private int quantity;
    private int securrityId;
    private TradeInstruction tradeInstruction;

    public RequestMessage(String request) {
        if (request == null) {
            throw new IllegalArgumentException("Request string is null.");
        }
        parseAndAssignInput(request);
    }

    private void parseAndAssignInput(final String input) {
        //Example: 123 BUY 100
        String[] splittedInput = input.split(MSG_DELIMITER);
        if (splittedInput.length != NUM_PARAMETERS) {
            throw new IllegalArgumentException("Exactly " + NUM_PARAMETERS + " space separated parameters expected");
        }
        this.tradeInstruction = TradeInstruction.valueOf(splittedInput[T_INSTRUCTION_INDEX].toUpperCase()); // throws illegal argument exception if not found
        this.securrityId = stringToInt(splittedInput[SECURITYID_INDEX], MIN_SECURITY_ID);
        this.quantity = stringToInt(splittedInput[QUANTITY_INDEX], MIN_QUANTITY);
    }

    private int stringToInt(final String val, final int min) {
        int result;
        try {
            result = Integer.parseInt(val);
            if (result < min) {
                throw new IllegalArgumentException("Security id must be > 0");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return result;
    }
}
