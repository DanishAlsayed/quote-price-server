package com.example.marketmaker;

import java.util.Random;

public class ReferencePrice implements ReferencePriceSource {
    private ReferencePriceSourceListener refPriceListener;
    private Random random;

    public ReferencePrice() {
        this.random = new Random();
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        if (listener == null) {
            return;
        }
        this.refPriceListener = listener;
    }

    @Override
    public double get(int securityId) {
        //fake price
        return random.nextInt(10) + 1;
    }

    public void startSource() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (random.nextInt(100) == 1) {
                refPriceListener.getSecurityIds().forEach(id -> refPriceListener.referencePriceChanged(id, get(id)));
            }
        }
    }
}
