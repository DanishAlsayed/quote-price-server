package com.example.marketmaker;

public class Quote implements QuoteCalculationEngine {
    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        //fake logic
        double spread = referencePrice / quantity; //assuming that the larger the quantity the more liquid the security is the smaller the spread
        double quote;
        if (buy) {
            quote = referencePrice - spread / 2;
        } else {
            quote = referencePrice + spread / 2;
        }
        return quote;
    }
}
