package com.example.marketmaker;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ReferencePriceListener implements ReferencePriceSourceListener {

    private Map<Integer, Double> refPrices = new ConcurrentHashMap<Integer, Double>();

    @Override
    public void referencePriceChanged(int securityId, double price) {
        //TODO: sanity checks
        refPrices.put(securityId, price); //TODO: only update if ref price is different that what we have
        System.out.println("referencePriceChanged: Security ID: " + securityId + " Price: " + price);
    }

    @Override
    public Set<Integer> getSecurityIds() {
        return refPrices.keySet();
    }

    @Override
    public void addSecurityId(int securityId) {
        if (!getSecurityIds().contains(securityId)) {
            refPrices.put(securityId, -1.0);
        }
    }

    @Override
    public double getRefPrice(int securityId) {
        if(refPrices == null){
            System.out.println("Quotes store is null!");
        }
        if (!getSecurityIds().contains(securityId)) {
            return -1.0;
        }
        return refPrices.get(securityId);
    }
}