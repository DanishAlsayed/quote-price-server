package com.example.marketmaker;

import java.util.Set;

/**
 * Callback interface for {@link ReferencePriceSource}
 */
public interface ReferencePriceSourceListener {

    /**
     * Called when a price has changed.
     *
     * @param securityId security identifier
     * @param price      reference price
     */
    void referencePriceChanged(int securityId, double price);

    Set<Integer> getSecurityIds();

    void addSecurityId(int securityId);

    double getRefPrice(int securityId);
}
