package test.java.com.example.quoteserver;

import com.example.quoteserver.RequestMessage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RequestMessageTest {
    private RequestMessage requestMessage = null;

    @Test
    public void buy() {
        requestMessage = new RequestMessage("123 BUY 100");
        assertEquals(requestMessage.getQuantity(), 100);
        assertEquals(requestMessage.getSecurrityId(), 123);
        assertEquals(requestMessage.getTradeInstruction(), RequestMessage.TradeInstruction.BUY);
    }

    @Test
    public void sell() {
        requestMessage = new RequestMessage("123 sell 100"); //note the small letters in "sell"
        assertEquals(requestMessage.getQuantity(), 100);
        assertEquals(requestMessage.getSecurrityId(), 123);
        assertEquals(requestMessage.getTradeInstruction(), RequestMessage.TradeInstruction.SELL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void extraParams() {
        requestMessage = new RequestMessage("123 sell 100 extra");
    }

    @Test(expected = IllegalArgumentException.class)
    public void lessParams() {
        requestMessage = new RequestMessage("123 sell");
    }

    @Test(expected = IllegalArgumentException.class)
    public void unknownTradeInstruction() {
        requestMessage = new RequestMessage("123 hold 100");
    }

    @Test(expected = IllegalArgumentException.class)
    public void stringQuantity() {
        requestMessage = new RequestMessage("123 BUY text");
    }

    @Test(expected = IllegalArgumentException.class)
    public void stringSecurityId() {
        requestMessage = new RequestMessage("test BUY 100");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullMessage() {
        requestMessage = new RequestMessage(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalQuantity() {
        requestMessage = new RequestMessage("123 buy 0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalSecurityId() {
        requestMessage = new RequestMessage("-1 buy 100");
    }
}
