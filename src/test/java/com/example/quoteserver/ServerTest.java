package test.java.com.example.quoteserver;

import com.example.marketmaker.Quote;
import com.example.quoteserver.Server;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerTest implements Runnable {
    private com.example.marketmaker.QuoteCalculationEngine qEngine = (com.example.marketmaker.QuoteCalculationEngine) new Quote();
    private Server server = new Server(qEngine);
    Thread serverThread = new Thread(this, "server");

    @Override
    public void run() {
        try {
            server.startServer();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static class TestClient implements Runnable {
        private String input;

        public TestClient(String input) {
            this.input = input;
        }

        String send() throws Exception {
            Socket clientSocket = new Socket("localhost", Server.PORT);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            outToServer.writeBytes(input + '\n');
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String result = inFromServer.readLine();
            clientSocket.close();
            return result;
        }

        @Override
        public void run() {
            try {
                System.out.println("QUOTE FROM SERVER: " + send());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Test
    public void serverTest() {
        Thread t = new Thread(new ServerTest());
        t.start();

        Thread t2 = new Thread(new TestClient("123 BUY 100"));
        t2.start();
        Thread t3 = new Thread(new TestClient("133 BUY 50"));
        t3.start();
        try {
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
